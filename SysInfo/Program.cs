﻿namespace SysInfo
{
  using System;
  using System.Diagnostics;
  using System.IO;
  using System.Management;

  public class Program
  {
    public static void Main(string[] args)
    {
      string userId = Environment.UserName;
      string machineName = string.Empty;
      string operatingSystem = string.Empty;
      long totalMemory = 0;
      long totalHardDisk = 0;

      try
      {
        ManagementObjectSearcher searcher = new ManagementObjectSearcher("SELECT Caption, CSName, TotalVisibleMemorySize FROM Win32_OperatingSystem");
        foreach (ManagementObject os in searcher.Get())
        {
          operatingSystem = os["Caption"].ToString();
          machineName = os["CSName"].ToString();
          totalMemory = long.Parse(os["TotalVisibleMemorySize"].ToString());
        }

        string printerDriverString = "{";
        searcher = new ManagementObjectSearcher("SELECT * FROM Win32_PrinterDriver");
        foreach (ManagementObject queryObj in searcher.Get())
        {
          if (!queryObj["Name"].ToString().Contains("Snagit")
            && !queryObj["Name"].ToString().Contains("Fax")
            && !queryObj["Name"].ToString().Contains("Microsoft Print To PDF")
            && !queryObj["Name"].ToString().Contains("Microsoft XPS")
            && !queryObj["Name"].ToString().Contains("OneNote")
            && !queryObj["Name"].ToString().Contains("Remote Desktop"))
          {
            printerDriverString += queryObj["Name"] + " / ";

            foreach (PropertyData property in queryObj.Properties)
            {
              if (property.Name == "DriverPath")
              {
                FileVersionInfo versionInfo = FileVersionInfo.GetVersionInfo(property.Value.ToString());
                printerDriverString += "Version: " + versionInfo.ProductVersion + ",";
              }
            }
          }
        }

        printerDriverString = printerDriverString.Substring(0, printerDriverString.Length - 1) + "}";

        DriveInfo[] drives = DriveInfo.GetDrives();
        foreach (DriveInfo drive in drives)
        {
          if (drive.Name == @"C:\")
          {
            totalHardDisk = drive.TotalSize;
          }
        }

        StreamWriter streamWriter = new StreamWriter("SysInfo.txt", true);
        streamWriter.WriteLine(string.Format("{0},{1},{2},{3},{4},{5}", userId, machineName, operatingSystem, totalHardDisk, totalMemory, printerDriverString));
        streamWriter.Flush();
        streamWriter.Close();

        Console.WriteLine("User Id: " + userId);
        Console.WriteLine("Operating System: " + operatingSystem);
        Console.WriteLine("Machine Name: " + machineName);
        Console.WriteLine("Total Memory: " + Math.Round(totalMemory / 1024.0 / 1024.0, 0) + " GB");
        Console.WriteLine("Total C Drive Space: " + Math.Round(totalHardDisk / 1024.0 / 1024.0, 0) + " GB");
      }
      catch (Exception exception)
      {
        Console.WriteLine("Error: " + exception.Message);
      }

      Console.WriteLine(string.Empty);
      Console.WriteLine("Finished, press enter to exit");
      Console.ReadLine();
    }
  }
}
